setMHSampler <- function(model, nodeName, proposal)
	setSampler(model, nodeName, "mh", list(proposal, environment(proposal), model[[nodeName]]$value))

setMHNormWalkSampler <- function(model, nodeName, width)
	setSampler(model, nodeName, "mh_norm_walk", list(as.real(width), model[[nodeName]]$value))

setAMHScalarNormWalkSampler <- function(model, nodeName, thin=50) {
	extra <- list(as.real(0), as.integer(0), as.integer(0), as.integer(thin), model[[nodeName]]$value);
	setSampler(model, nodeName, "amh_snorm_walk", extra)
}
