source("Test.R")

set.seed(1234)
A <- crossprod(matrix(rnorm(10), 5))

rgs_solve <- function(a) {
	ans <- matrix(0.0, nrow(a), ncol(a))
	.Call("rgs_matrix_solve", A, ans)
	ans
}
stopifnot(all.equal(rgs_solve(A), solve(A)))

A <- crossprod(matrix(rnorm(16), 4))
stopifnot(all.equal(.Call("rgs_modLa_chol", A), chol(A)))

rgs_logdet <- function(A) {
	ans <- as.real(0.0)
	A2 <- matrix(A)
	.Call("rgs_moddet_ge_real", A2, as.integer(numeric(nrow(A))), ans)
	ans
}
stopifnot(rgs_logdet(A) == log(det(A)))
