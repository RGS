#include <R_ext/Rdynload.h>
#include "rgs_node.h"
#include "rgs_distrib.h"

SEXP rgs_getValue(SEXP obj) {
	return RGS_NVALUE(obj);
}
SEXP rgs_setValue(SEXP obj, SEXP newValue) {
	if(length(newValue) != length(RGS_NVALUE(obj)))
		error("new value and old value size does not match");
	RGS_NSETVALUE(obj, REAL(newValue));
	return newValue;
}

SEXP rgs_distrib(SEXP obj) {
	return RGS_NDISTRIB(obj);
}
SEXP rgs_setdistrib(SEXP obj, SEXP newValue) {
	RGS_NSETDISTRIB(obj, newValue);
	return newValue;
}

SEXP rgs_parameters(SEXP obj) {
	return RGS_NPARAMETERS(obj);
}
SEXP rgs_setparameters(SEXP obj, SEXP newValue) {
	RGS_NSETPARAMETERS(obj, newValue);
	return newValue;
}

SEXP rgs_extra(SEXP obj) {
	return RGS_NEXTRA(obj);
}
SEXP rgs_setextra(SEXP obj, SEXP newValue) {
	RGS_NSETEXTRA(obj, newValue);
	return newValue;
}

SEXP rgs_sampler(SEXP obj) {
	return RGS_NSAMPLER(obj);
}
SEXP rgs_setsampler(SEXP obj, SEXP newValue) {
	RGS_NSETSAMPLER(obj, newValue);
	return newValue;
}

SEXP rgs_cachel(SEXP obj) {
	return RGS_NCACHEL(obj);
}
SEXP rgs_setCachel(SEXP obj, SEXP lst) {
	RGS_NSETCACHEL(obj, lst);
	return lst;
}

double rgs_compdistrib(SEXP obj) {
	SEXP pars = RGS_CPARAMETERSL(obj);
	SEXP par;
	for(int i=0; i < length(pars); i++) {
		par = RGS_CPARAMETERS(obj, i);
		if(!RGS_CDISTRIB(par)) /*if a parameter is a logical node, update it*/
			rgs_update(par);
	}
	return RGS_CDISTRIB(obj)(obj);
}

SEXP rgs_getdistrib(SEXP obj) {
	SEXP ans;
	PROTECT(ans = allocVector(REALSXP, 1));
	REAL(ans)[0] = rgs_compdistrib(obj);
	UNPROTECT(1);
	return ans;
}

double rgs_lik(SEXP obj) {
	double ans = 0.0;
	int nchilds = length(RGS_CCHILDSL(obj));
	SEXP child;
	for(int i=0; i < nchilds; i++) {
		child = RGS_CCHILDS(obj, i);
		/*if distribution is null, this is a logical node: to trough childs*/
		if(!RGS_CDISTRIB(child)) {
			ans += rgs_lik(child);
		}	else
			ans += rgs_compdistrib(child);
	}
	return ans;
}

SEXP rgs_getlik(SEXP obj) {
	SEXP ans;
	PROTECT(ans = allocVector(REALSXP, 1));
	REAL(ans)[0] = rgs_lik(obj);
	UNPROTECT(1);
	return ans;
}

double rgs_post(SEXP obj) {
	return RGS_CDISTRIB(obj)(obj) + rgs_lik(obj);
}

SEXP rgs_getpost(SEXP obj) {
	SEXP ans;
	PROTECT(ans = allocVector(REALSXP, 1));
	REAL(ans)[0] = rgs_post(obj);
	UNPROTECT(1);
	return ans;
}

void rgs_Rsampler(SEXP node) {
	SEXP samplerArgs = VECTOR_ELT( RGS_NEXTRA(node) , 1 );
	SEXP rfun = VECTOR_ELT(samplerArgs, 0);
	SEXP rho = VECTOR_ELT(samplerArgs, 1);
	eval(lang1( rfun ), rho);
}

void rgs_update(SEXP obj) {
	if(RGS_CSAMPLER(obj))
		RGS_CSAMPLER(obj)(obj);
}

SEXP rgs_getupdate(SEXP obj) {
	rgs_update(obj);
	return R_NilValue;
}
