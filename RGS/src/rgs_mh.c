#include "rgs_mh.h"

double unif_rand();
double norm_rand();

/*says if proposal value has to be rejected,
  basing on logarithms of old value and new value probabilities*/
int rgs_mh_dontjump(double oldP, double newP) {
	double logratio = newP - oldP;
	if(ISNAN(oldP))
		return 0;
	if(ISNAN(logratio))
		return 1;
	if(logratio < 0)
		return (unif_rand() > exp(logratio));
	return 0;
}

void rgs_mh(SEXP node) {
	SEXP proposalArgs = VECTOR_ELT( RGS_NEXTRA(node) , 1 );
	SEXP rfun = VECTOR_ELT(proposalArgs, 0);
	SEXP rho = VECTOR_ELT(proposalArgs, 1);
	SEXP cache = VECTOR_ELT(proposalArgs, 2);
	double oldP, newP, ratio;
	oldP = rgs_post(node);
	memcpy(REAL(cache), REAL(RGS_NVALUE(node)), length(RGS_NVALUE(node)) * sizeof(double) );
	eval(lang1( rfun ), rho);
	newP = rgs_post(node);
	ratio = newP - oldP;
	if(rgs_mh_dontjump(oldP, newP))
		memcpy(REAL(RGS_NVALUE(node)), REAL(cache), length(RGS_NVALUE(node)) * sizeof(double));
}

/*MH with normal random walk*/
void rgs_mh_norm_walk(SEXP node) {
	SEXP proposalArgs = VECTOR_ELT( RGS_NEXTRA(node) , 1 );
	double width = REAL( VECTOR_ELT(proposalArgs, 0) ) [0];
	SEXP cache = VECTOR_ELT(proposalArgs, 1);
	double oldP, newP, ratio;
	oldP = rgs_post(node);
	memcpy(REAL(cache), REAL(RGS_NVALUE(node)), length(RGS_NVALUE(node)) * sizeof(double) );
	for(int i=0; i<length( RGS_NVALUE(node) ); i++)
		REAL( RGS_NVALUE(node) )[i] += norm_rand() * width;
	newP = rgs_post(node);
	ratio = newP - oldP;
	if(rgs_mh_dontjump(oldP, newP))
		memcpy(REAL(RGS_NVALUE(node)), REAL(cache), length(RGS_NVALUE(node)) * sizeof(double));
}

/*Adaptive MH with (scalar) normal random walk*/
void rgs_amh_snorm_walk(SEXP node) {
	SEXP extra = VECTOR_ELT( RGS_NEXTRA(node) , 1 );
	double *ls = REAL( VECTOR_ELT(extra, 0) ); /*current log-sigma*/
	int *nreject = INTEGER(VECTOR_ELT(extra, 1)); /*current number of rejections*/
	int *ntot = INTEGER(VECTOR_ELT(extra, 2)); /*current iteration number*/
	int adaptThin = INTEGER(VECTOR_ELT(extra, 3))[0]; /*batch size*/
	SEXP cache = VECTOR_ELT(extra, 4);
	double oldP, newP, ratio;
	(*ntot)++;
	oldP = rgs_post(node);
	memcpy(REAL(cache), REAL(RGS_NVALUE(node)), length(RGS_NVALUE(node)) * sizeof(double) ); /*store current node value*/
	for(int i=0; i<length( RGS_NVALUE(node) ); i++)
		REAL( RGS_NVALUE(node) )[i] += norm_rand() * exp(*ls);
	newP = rgs_post(node);
	ratio = newP - oldP;
	if(rgs_mh_dontjump(oldP, newP)) {
		(*nreject)++;
		memcpy(REAL(RGS_NVALUE(node)), REAL(cache), length(RGS_NVALUE(node)) * sizeof(double));
	}
	/*perform adaptation*/
	if(!((*ntot) % adaptThin)) {
		int nbatch = (*ntot) / adaptThin;
		double step = 1.0 / sqrt( (double) nbatch );
		if(step > 0.01) step = 0.01;
		if(((double)(*nreject)/ (double)(*ntot)) > 0.56)
			(*ls) -= step;
		else
			(*ls) += step;
	}
}
