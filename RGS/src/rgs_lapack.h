#ifndef __RGS_LAPACK__
#define __RGS_LAPACK__

#include <Rdefines.h>
#include <Rinternals.h>
#include <R_ext/Lapack.h>
#include "rgs_misc.h"
#include "rgs_node.h"

SEXP rgs_eigen_symm_ask(SEXP xin);
SEXP rgs_eigen_symm(SEXP xin, SEXP in_work, SEXP in_iwork, SEXP out_values, SEXP out_vectors);

void rgs_matprod(double *x, int nrx, int ncx, double *y, int nry, int ncy, double *z);
void rgs_matrix_prod(SEXP a, SEXP b, SEXP ans);
/*log-determinant of a generic, real matrix*/
double rgs_logdet(SEXP Ain);

SEXP rgs_modLa_dgesv(SEXP A, SEXP B);
SEXP rgs_matrix_solve(SEXP a, SEXP ans);

/*cholesky decomposition*/
SEXP rgs_modLa_chol(SEXP A);

#endif
