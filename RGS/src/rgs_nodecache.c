#include <R_ext/Rdynload.h>
#include "rgs_misc.h"
#include "rgs_node.h"

/*refresh node cache*/
SEXP rgs_refreshCache(SEXP model, SEXP nodeNames, SEXP nodeChildsNames) {
	SEXP tmp; /*here only for better readability*/
	SEXP node, nodeName, nodeChilds;
	for(int i=0; i<length(nodeNames); i++) {
		nodeName = VECTOR_ELT(nodeNames, i);
		nodeChilds = VECTOR_ELT(nodeChildsNames, i);
		node = getListElement(model, CHAR(VECTOR_ELT(nodeName, 0)));
		RGS_NSETCACHEL(node, allocVector(VECSXP, 4)); /*alloc a new cache vector*/
		/*set distribution pointer*/
		tmp = R_MakeExternalPtr(R_FindSymbol(CHAR( VECTOR_ELT(RGS_NDISTRIB(node), 0) ), "", NULL), R_NilValue, R_NilValue);
		RGS_NSETCACHE(node, 0, tmp);
		/*set parameters pointers*/
		RGS_NSETCACHE(node, 1, allocVector(VECSXP, length(RGS_NPARAMETERS(node))));
		for(int i=0; i<length(RGS_NPARAMETERS(node)); i++) {
			tmp = getListElement(model, CHAR(VECTOR_ELT(RGS_NPARAMETERS(node), i)));
			SET_VECTOR_ELT( RGS_NCACHE(node, 1), i, R_MakeExternalPtr(tmp, R_NilValue, R_NilValue) );
		}
		/*set childs pointers*/
		RGS_NSETCACHE(node, 2, allocVector(VECSXP, length(nodeChilds)));
		for(int i=0; i<length(nodeChilds); i++) {
			tmp = getListElement(model, CHAR(VECTOR_ELT(nodeChilds, i)));
			SET_VECTOR_ELT( RGS_NCACHE(node, 2), i, R_MakeExternalPtr(tmp, R_NilValue, R_NilValue) );
		}
		/*set sampler pointer*/
		tmp = R_MakeExternalPtr(R_FindSymbol(CHAR( VECTOR_ELT(RGS_NSAMPLER(node), 0) ), "", NULL), R_NilValue, R_NilValue);
		RGS_NSETCACHE(node, 3, tmp);
	}
	return model;
}

SEXP rgs_testCache(SEXP model, SEXP nodeNames, SEXP childsList) {
	/*refresh all nodes cache*/
	for(int i=0; i<length(nodeNames); i++)
		rgs_refreshCache(model, VECTOR_ELT(nodeNames, i), VECTOR_ELT(childsList, i));
	/*check that all is consistent*/
	Rprintf("a: %p\tb->a:%p\n", VECTOR_ELT(model, 0), RGS_CPARAMETERS( VECTOR_ELT(model, 1), 0 ));
	return R_NilValue;
}

SEXP rgs_cdistrib(SEXP obj) {
	SEXP ans;
	char *a = R_alloc(2+sizeof(SEXP) * 2, sizeof(char)); /*'0x' + 2 characters per byte*/
	sprintf(a, "%p", RGS_CDISTRIB(obj));
	PROTECT(ans = allocVector(STRSXP, 1));
	SET_VECTOR_ELT(ans, 0, mkChar(a));
	UNPROTECT(1);
	return ans;
}

SEXP rgs_cparameters(SEXP obj, SEXP n) {
	return rgs_getPtr(RGS_CPARAMETERS(obj, INTEGER(n)[0]));
}

SEXP rgs_cchildsl(SEXP obj) {
	return RGS_CCHILDSL(obj);
}

SEXP rgs_cchilds(SEXP obj, SEXP n) {
	return rgs_getPtr(RGS_CCHILDS(obj, INTEGER(n)[0]));
}
