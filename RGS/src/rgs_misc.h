#ifndef __RGS_MISC__
#define __RGS_MISC__


#include <Rdefines.h>
#include <Rinternals.h>

SEXP rgs_getPtr(SEXP obj);
SEXP getListElement(SEXP list, const char *str);

#endif
