#include "rgs_lapack.h"

/* adapted from the R sources*/
/* ask for optimal size of work arrays
 returns a list of 2 integers: 'lwork' and 'liwork', containing
 space to be allocated for subsequent (symmetric matrices) eigenvalues computation*/
SEXP rgs_eigen_symm_ask(SEXP xin) {
	int *xdims, n, lwork, liwork, ov, info;
	char jobv[1], uplo[1], range[1];
	SEXP values, ret, nm, x, z = R_NilValue;
	double *work, *rx, *rvalues, tmp, *rz = NULL;
	int *iwork, itmp, m;
	double vl = 0.0, vu = 0.0, abstol = 0.0;
	int il, iu, *isuppz;
	
	x = xin;
	rx = REAL(x);
	uplo[0] = 'L';
	xdims = INTEGER(coerceVector(getAttrib(x, R_DimSymbol), INTSXP));
	n = xdims[0];
	ov = 0;
	jobv[0] = 'V';

	PROTECT(values = allocVector(REALSXP, n));
	rvalues = REAL(values);
	
	range[0] = 'A';
	PROTECT(z = allocMatrix(REALSXP, n, n));
	rz = REAL(z);
	isuppz = (int *) R_alloc(2*n, sizeof(int));
	lwork = -1; liwork = -1;
	F77_CALL(dsyevr)(jobv, range, uplo, &n, rx, &n,
		&vl, &vu, &il, &iu, &abstol, &m, rvalues,
		rz, &n, isuppz,
		&tmp, &lwork, &itmp, &liwork, &info);
	lwork = (int) tmp;
	liwork = itmp;
	
	PROTECT(ret = allocVector(VECSXP, 2));
	SET_VECTOR_ELT(ret, 0, allocVector(INTSXP, 1));
	INTEGER(VECTOR_ELT(ret, 0))[0] = lwork;
	SET_VECTOR_ELT(ret, 1, allocVector(INTSXP, 1));
	INTEGER(VECTOR_ELT(ret, 1))[0] = liwork;
	UNPROTECT(3);
	return ret;
}

/*eigenvalues and vectors of a symmetric matrix*/
SEXP rgs_eigen_symm(SEXP xin, SEXP in_work, SEXP in_iwork, SEXP out_values, SEXP out_vectors) {
	int *xdims, n, lwork, info = 0, ov;
	char jobv[1], uplo[1], range[1];
	SEXP values, ret, nm, x, z = R_NilValue;
	double *work, *rx, *rvalues, tmp, *rz = NULL;
	int liwork, *iwork, itmp, m;
	double vl = 0.0, vu = 0.0, abstol = 0.0;
	/* valgrind seems to think vu should be set, but it is documented
			not to be used if range='a' */
	int il, iu, *isuppz;

	x = xin;
	rx = REAL(x);
	uplo[0] = 'L';
	xdims = INTEGER(coerceVector(getAttrib(x, R_DimSymbol), INTSXP));
	n = xdims[0];
	ov = 0;
	jobv[0] = 'V';

	values = out_values;
	rvalues = REAL(values);

	range[0] = 'A';
	z = out_vectors;
	rz = REAL(z);

	isuppz = INTEGER(in_iwork) + (2*n);
	lwork = length(in_work);
	liwork = length(in_iwork);

	work = REAL(in_work);
	iwork = INTEGER(in_iwork);
	F77_CALL(dsyevr)(jobv, range, uplo, &n, rx, &n,
		&vl, &vu, &il, &iu, &abstol, &m, rvalues,
		rz, &n, isuppz,
		work, &lwork, iwork, &liwork, &info);
	if (info != 0)
		error("error code %d from Lapack routine '%s'", info, "dsyevr");
	return R_NilValue;
}

/*adapted from 'matprod' in R sources*/
void rgs_matprod(double *x, int nrx, int ncx,
		    double *y, int nry, int ncy, double *z) {
	char *transa = "N", *transb = "N";
	int i,  j, k;
	double one = 1.0, zero = 0.0;
	double sum;
	Rboolean have_na = FALSE;

	if (nrx > 0 && ncx > 0 && nry > 0 && ncy > 0) {
	/* Don\'t trust the BLAS to handle NA/NaNs correctly: PR#4582
	 * The test is only O(n) here
	 */
		for (i = 0; i < nrx*ncx; i++)
				if (ISNAN(x[i])) {have_na = TRUE; break;}
		if (!have_na)
				for (i = 0; i < nry*ncy; i++)
			if (ISNAN(y[i])) {have_na = TRUE; break;}
		if (have_na) {
				for (i = 0; i < nrx; i++)
			for (k = 0; k < ncy; k++) {
					sum = 0.0;
					for (j = 0; j < ncx; j++)
				sum += x[i + j * nrx] * y[j + k * nry];
					z[i + k * nrx] = sum;
			}
		} else
			F77_CALL(dgemm)(transa, transb, &nrx, &ncy, &ncx, &one,
			x, &nrx, y, &nry, &zero, z, &nrx);
	} else /* zero-extent operations should return zeroes */
		for(i = 0; i < nrx*ncy; i++) z[i] = 0;
}

/*matrix product*/
void rgs_matrix_prod(SEXP a, SEXP b, SEXP ans) {
	int *adims, *bdims;
	adims = INTEGER(coerceVector(getAttrib(a, R_DimSymbol), INTSXP));
	bdims = INTEGER(coerceVector(getAttrib(b, R_DimSymbol), INTSXP));
	rgs_matprod(REAL(a), adims[0], adims[1],
		REAL(b), bdims[0], bdims[1], REAL(ans));
}

SEXP rgs_modLa_dgesv(SEXP A, SEXP B) {
	int n, p, info, *ipiv, *Adims, *Bdims;
	double *avals, anorm, rcond, *work;
	double tol=1e-6;

	Adims = INTEGER(coerceVector(getAttrib(A, R_DimSymbol), INTSXP));
	Bdims = INTEGER(coerceVector(getAttrib(B, R_DimSymbol), INTSXP));
	n = Adims[0];
	p = Bdims[1];
	ipiv = (int *) R_alloc(n, sizeof(int));

	avals = (double *) R_alloc(n * n, sizeof(double));
	/* work on a copy of A */
	Memcpy(avals, REAL(A), (size_t) (n * n));
	F77_CALL(dgesv)(&n, &p, avals, &n, ipiv, REAL(B), &n, &info);
	if (info < 0)
		error("argument %d of Lapack routine %s had invalid value", -info, "dgesv");
	if (info > 0)
		error("Lapack routine dgesv: system is exactly singular");
	anorm = F77_CALL(dlange)("1", &n, &n, REAL(A), &n, (double*) NULL);
	work = (double *) R_alloc(4*n, sizeof(double));
	F77_CALL(dgecon)("1", &n, avals, &n, &anorm, &rcond, work, ipiv, &info);
	if (rcond < tol)
		error("system is computationally singular: reciprocal condition number = %g", rcond);
	return B;
}

/*matrix inversion*/
SEXP rgs_matrix_solve(SEXP a, SEXP ans) {
	int n = INTEGER(coerceVector(getAttrib(a, R_DimSymbol), INTSXP))[0];
	memset(REAL(ans), 0, n * n * sizeof(double));
	for(int i=0; i<n; i++)
		REAL(ans)[i + i * n] = 1.0;
	rgs_modLa_dgesv(a, ans);
	return R_NilValue;
}

/*Choleski decomposition.
  Adapted from R sources*/
SEXP rgs_modLa_chol(SEXP A) {
	SEXP ans = PROTECT((TYPEOF(A) == REALSXP)?duplicate(A): coerceVector(A, REALSXP));
	SEXP adims = getAttrib(A, R_DimSymbol);
	int m = INTEGER(adims)[0];
	int n = INTEGER(adims)[1];
	int i, j;

	for (j = 0; j < n; j++) {	/* zero the lower triangle */
		for (i = j+1; i < n; i++) {
			REAL(ans)[i + j * n] = 0.;
		}
	}

	F77_CALL(dpotrf)("Upper", &m, REAL(ans), &m, &i);
	if (i != 0) {
		if (i > 0)
			error("the leading minor of order %d is not positive definite", i);
		error("argument %d of Lapack routine %s had invalid value", -i, "dpotrf");
	}
	unprotect(1);
	return ans;
}

/* log-determinant of a generic real matrix, adapted from R sources
	A: square matrix n by n
	in_jptv: length n integer vector
	ans: scalar real*/
SEXP rgs_moddet_ge_real(SEXP A, SEXP in_jptv, SEXP ans) {
	int i, n, info, *jpvt, sign;
	double modulus = 0.0; /* -Wall */
	SEXP val, nm;

	n = length(in_jptv);
	jpvt = INTEGER(in_jptv);
	F77_CALL(dgetrf)(&n, &n, REAL(A), &n, jpvt, &info);
	sign = 1;
	if (info < 0)
		error("error code %d from Lapack routine '%s'", info, "dgetrf");
	else if (info > 0) {
		modulus = R_NegInf;
	}
	else {
		for (i = 0; i < n; i++) if (jpvt[i] != (i + 1))
		sign = -sign;
		modulus = 0.0;
		for (i = 0; i < n; i++) {
			double dii = REAL(A)[i*(n + 1)]; /* ith diagonal element */
			modulus += log(dii < 0 ? -dii : dii);
			if (dii < 0) sign = -sign;
		}
	}
	REAL(ans)[0] = modulus * ((double) sign);
	return R_NilValue;
}

/*log-determinant of a generic, real matrix*/
double rgs_logdet(SEXP Ain) {
	SEXP A, jptv, ans;
	int n;
	PROTECT(A = duplicate(Ain));
	n = INTEGER(coerceVector(getAttrib(A, R_DimSymbol), INTSXP))[0];
	PROTECT(jptv = allocVector(INTSXP, n));
	PROTECT(ans = allocVector(REALSXP, 1));
	UNPROTECT(3);
	rgs_moddet_ge_real(A, jptv, ans);
	return REAL(ans)[0];
}
