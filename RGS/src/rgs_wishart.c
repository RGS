#include <Rmath.h>
#include "rgs_distrib.h"
#include "rgs_lapack.h"

double norm_rand();

SEXP rgs_rwishart(SEXP in_nu, SEXP V, SEXP out_ans) {
	int *dims;
	SEXP U;
	dims = INTEGER(coerceVector(getAttrib(V, R_DimSymbol), INTSXP));
	int m = dims[0];
	int nu = INTEGER(in_nu)[0];
	int df = nu;
	SEXP tmp1, tmp2;
	PROTECT(tmp1 = duplicate(out_ans));
	PROTECT(tmp2 = duplicate(out_ans));
	double *ans = REAL(tmp1);
	
	for(int i=0; i<m; i++) {
		/*(square-root of) chi-squared variates on the diagonal*/
		ans[i + i*m] = sqrt(rchisq((double) (df - i)));
		/*standard normal variates out of diagonal*/
		for(int j=0; j<i; j++)
			ans[i + j*m] = norm_rand();
	}
	/*choleski decomposition*/
	PROTECT(U = rgs_modLa_chol(V));

	char *transa = "T", *transb = "N";
	double one = 1.0, zero = 0.0;
	F77_CALL(dgemm)(transa, transb, &m, &m, &m, &one,
		ans, &m, REAL(U), &m, &zero, REAL(tmp2), &m);
	F77_CALL(dgemm)(transa, transb, &m, &m, &m, &one,
		REAL(tmp2), &m, REAL(tmp2), &m, &zero, REAL(out_ans), &m);

	UNPROTECT(3);
	return R_NilValue;
}

SEXP rgs_dwishart_work(SEXP W, SEXP nu, SEXP S, SEXP ans) {
	int *dims = INTEGER(coerceVector(getAttrib(S, R_DimSymbol), INTSXP));
	double v = (double) (INTEGER(nu)[0]);
	int k = dims[0];
	double num, denom, detS, detW, tracehold;
	double gammapart = 0.0;
	SEXP tmp_hold, hold;
	for(int i=1; i<=k; i++)
		gammapart += log(gammafn( (v + 1.0 - ((double) i)) / 2.0 ));
	denom = gammapart +  v * ((double) k) * log(2) / 2.0;
	denom += ((double)(k * (k-1))) * log(PI) /4.0;

	detS = rgs_logdet(S);
	detW = rgs_logdet(W);

	PROTECT(tmp_hold = duplicate(S));
	PROTECT(hold = duplicate(S));
	rgs_matrix_solve(S, tmp_hold);
	rgs_matrix_prod(tmp_hold, W, hold);
	tracehold = 0.0;
	for(int i=0; i<k; i++)
		tracehold += REAL(hold)[i + i*k];
	num = -v * detS / 2.0 + ((double)(v-k-1.0)) * detW / 2.0 -0.5 * tracehold;
	UNPROTECT(2);
	REAL(ans)[0] = num - denom;
	return R_NilValue;
}

double rgs_dwishart(SEXP node){
	SEXP S, ans;
	SEXP nu = RGS_NVALUE( RGS_CPARAMETERS(node, 0) );
	S = RGS_NVALUE( RGS_CPARAMETERS(node, 1) );
	PROTECT( ans = allocVector(REALSXP, 1));
	SEXP W = RGS_NVALUE(node);
	rgs_dwishart_work(W, nu, S, ans);
	UNPROTECT(1);
	return REAL(ans)[0];
}
