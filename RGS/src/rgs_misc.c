#include "rgs_misc.h"

/*return C ptr (as a string) to given R obj*/
SEXP rgs_getPtr(SEXP obj) {
	SEXP ans;
	char *a = R_alloc(2+sizeof(SEXP) * 2, sizeof(char)); /*'0x' + 2 characters per byte*/
	sprintf(a, "%p", obj);
	PROTECT(ans = allocVector(STRSXP, 1));
	SET_VECTOR_ELT(ans, 0, mkChar(a));
	UNPROTECT(1);
	return ans;
}

SEXP getListElement(SEXP list, const char *str) {
	SEXP elmt = R_NilValue, names = getAttrib(list, R_NamesSymbol);
	for (int i = 0; i < length(list); i++)
		if(strcmp(CHAR(STRING_ELT(names, i)), str) == 0) {
			elmt = VECTOR_ELT(list, i);
			break;
		}
	return elmt;
}
