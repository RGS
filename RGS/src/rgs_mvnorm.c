#include "rgs_distrib.h"
#include "rgs_lapack.h"

#define PI 3.14159265358979323846

double rgs_dmvnorm(SEXP node) {
	double ans=0.0;
	SEXP mu = RGS_NVALUE( RGS_CPARAMETERS(node, 0) );
	SEXP Sigma;
	PROTECT( Sigma = duplicate( RGS_NVALUE( RGS_CPARAMETERS(node, 1) ) ) );
	SEXP Sigma_inv;
	SEXP x = RGS_NVALUE(node);
	int n = length(x);
	SEXP demeaned, distval, eigvals, eigvects;
	SEXP tmplist, work, iwork;
	PROTECT( demeaned = allocMatrix(REALSXP, 1, n) );
	for(int i=0; i<n; i++)
		REAL(demeaned)[i] = REAL(x)[i] - REAL(mu)[i];

	PROTECT( Sigma_inv = allocMatrix(REALSXP, n, n) );
	rgs_matrix_solve(Sigma, Sigma_inv);
	PROTECT( distval = allocMatrix(REALSXP, 1, n) );
	rgs_matrix_prod(demeaned, Sigma_inv, distval);
	for(int i=0; i<n; i++)
		ans += REAL(distval)[i] * REAL(demeaned)[i];
	/*now 'ans' contains t(x-mu) %*% solve(Sigma) %*% (x-mu) */

	PROTECT(eigvals = allocVector(REALSXP, n));
	PROTECT(eigvects = allocMatrix(REALSXP, n, n));
	PROTECT(tmplist = rgs_eigen_symm_ask(Sigma));
	PROTECT(work = allocVector(REALSXP, INTEGER(VECTOR_ELT(tmplist, 0))[0] ));
	PROTECT(iwork = allocVector(INTSXP, INTEGER(VECTOR_ELT(tmplist, 1))[0] + n ));
	rgs_eigen_symm(Sigma, work, iwork, eigvals, eigvects);
	for(int i=0; i<n; i++)
		ans += log(REAL(eigvals)[i]);
	/*now to 'ans' the log-determinant has been summed*/

	ans += n * log(2 * PI);
	/*normalizing constant*/

	UNPROTECT(9);
	return -ans/2.0;
}

double norm_rand();

SEXP rgs_rmvnorm(SEXP mu, SEXP in_Sigma, SEXP ans) {
	SEXP Sigma;
	PROTECT(Sigma = duplicate( in_Sigma ));
	int n = length(mu);
	SEXP eigvals, eigvects, tmplist, work, iwork;
	SEXP tmp1, tmp2;

	PROTECT(eigvals = allocVector(REALSXP, n));
	PROTECT(eigvects = allocMatrix(REALSXP, n, n));
	PROTECT(tmplist = rgs_eigen_symm_ask(Sigma));
	PROTECT(work = allocVector(REALSXP, INTEGER(VECTOR_ELT(tmplist, 0))[0] ));
	PROTECT(iwork = allocVector(INTSXP, INTEGER(VECTOR_ELT(tmplist, 1))[0] + n ));
	rgs_eigen_symm(Sigma, work, iwork, eigvals, eigvects);

	PROTECT(tmp2 = allocMatrix(REALSXP, n, n));
	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++)
			REAL(tmp2)[i + j*n] = sqrt(REAL(eigvals)[n-j-1]) * REAL(eigvects)[i + (n-j-1)*n];

	PROTECT(tmp1 = allocMatrix(REALSXP, n, 1)); /*generate a normal iid vector*/
	for(int i=0; i<n; i++)
		REAL(tmp1)[i] = norm_rand();

	rgs_matrix_prod(tmp2, tmp1, ans); /*rotate and scale the iid vector*/

	for(int i=0; i<n; i++) /*add mean*/
		REAL(ans)[i] += REAL(mu)[i];

	UNPROTECT(8);
	return R_NilValue;
}
