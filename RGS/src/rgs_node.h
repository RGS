#ifndef __RGS_NODE__
#define __RGS_NODE__

#include <Rdefines.h>
#include <Rinternals.h>
#include "rgs_misc.h"
#include "rgs_distrib.h"

typedef void (*pt2VFunc)(SEXP);

#define RGS_NVALUE(obj) VECTOR_ELT((obj), 0)
#define RGS_NRVALUE(obj) REAL(RGS_NVALUE((obj)))
#define RGS_NSETVALUE(obj, newValue) \
  memcpy(RGS_NRVALUE((obj)), (newValue), length(RGS_NVALUE((obj))) * sizeof(double));
#define RGS_NDISTRIB(obj) VECTOR_ELT((obj), 1)
#define RGS_NSETDISTRIB(obj, newValue) SET_VECTOR_ELT((obj), 1, (newValue))
#define RGS_NPARAMETERS(obj) VECTOR_ELT((obj), 2)
#define RGS_NSETPARAMETERS(obj, newValue) SET_VECTOR_ELT((obj), 2, (newValue))
#define RGS_NEXTRA(obj) VECTOR_ELT((obj), 3)
#define RGS_NSETEXTRA(obj, newValue) SET_VECTOR_ELT((obj), 3, (newValue))
#define RGS_NSAMPLER(obj) VECTOR_ELT((obj), 5)
#define RGS_NSETSAMPLER(obj, newValue) SET_VECTOR_ELT((obj), 5, (newValue))

#define RGS_NCACHEL(obj) VECTOR_ELT((obj), 4)
#define RGS_NSETCACHEL(obj, lst) SET_VECTOR_ELT((obj), 4, (lst))
#define RGS_NCACHE(obj, n) VECTOR_ELT(RGS_NCACHEL((obj)), (n))
#define RGS_NSETCACHE(obj, n, value) SET_VECTOR_ELT(RGS_NCACHEL((obj)), (n), (value))
#define RGS_CDISTRIB(obj) \
	((pt2Func) R_ExternalPtrAddr( RGS_NCACHE((obj), 0) ))
#define RGS_CPARAMETERSL(obj) RGS_NCACHE((obj), 1)
#define RGS_CPARAMETERS(obj, n) \
	((SEXP) R_ExternalPtrAddr(VECTOR_ELT( RGS_CPARAMETERSL(obj), (n) )))
#define RGS_CCHILDSL(obj) \
	RGS_NCACHE((obj), 2)
#define RGS_CCHILDS(obj, n) \
	(SEXP) R_ExternalPtrAddr(VECTOR_ELT( RGS_CCHILDSL((obj)), (n) ))
#define RGS_CSAMPLER(obj) \
	((pt2VFunc) R_ExternalPtrAddr( RGS_NCACHE((obj), 3) ))

SEXP rgs_getValue(SEXP obj);
SEXP rgs_setValue(SEXP obj, SEXP newValue);
SEXP rgs_parameters(SEXP obj);
SEXP rgs_setparameters(SEXP obj, SEXP newValue);
SEXP rgs_distrib(SEXP obj);
SEXP rgs_setdistrib(SEXP obj, SEXP newValue);
SEXP rgs_extra(SEXP obj);
SEXP rgs_setextra(SEXP obj, SEXP newValue);
SEXP rgs_sampler(SEXP obj);
SEXP rgs_setsampler(SEXP obj, SEXP newValue);

SEXP rgs_cachel(SEXP obj);
SEXP rgs_setCachel(SEXP obj, SEXP lst);

void rgs_update(SEXP obj);
double rgs_post(SEXP obj);

#endif
