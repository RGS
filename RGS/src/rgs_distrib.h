#ifndef __RGS_DISTRIB__
#define __RGS_DISTRIB__
#include <Rinternals.h>

#define PI 3.14159265358979323846

#define RGS_DPAR(node, n) REAL( RGS_NVALUE( RGS_CPARAMETERS((node), (n)) ) )
#define RGS_DPAR0 RGS_DPAR(node, 0)
#define RGS_DPAR1 RGS_DPAR(node, 1)
#define RGS_DPAR2 RGS_DPAR(node, 2)
#define RGS_VAL REAL( RGS_NVALUE(node))

typedef double (*pt2Func)(SEXP);

double rgs_dnorm(SEXP node);
double rgs_dgamma(SEXP node);
double rgs_dbeta(SEXP node);
double rgs_dchisq(SEXP node);
double rgs_dexp(SEXP node);
double rgs_dlogis(SEXP node);
double rgs_dbinom(SEXP node);
double rgs_dgeom(SEXP node);
double rgs_dunif(SEXP node);
double rgs_dpois(SEXP node);
double rgs_df(SEXP node);

#endif
