#include <Rmath.h>
#include "rgs_node.h"
#include "rgs_distrib.h"

double rgs_dnorm(SEXP node) {
	double ans = 0.0;
	for(int i=0; i< length(RGS_NVALUE(node)); i++)
		ans += dnorm(RGS_VAL[i], RGS_DPAR0[0], R_pow( RGS_DPAR1[0], -0.5), 1);
	return ans;
}

double rgs_dgamma(SEXP node) {
	return dgamma(RGS_VAL[0], /*shape*/RGS_DPAR0[0], /*rate*/ RGS_DPAR1[0], 1); }

double rgs_dcauchy(SEXP node) {
       return dcauchy(RGS_VAL[0], RGS_DPAR0[0], RGS_DPAR1[0], 1);}

double rgs_dbeta(SEXP node) {
	return dbeta(RGS_VAL[0], /*shape1*/ RGS_DPAR0[0], /*shape2*/ RGS_DPAR1[0], 1); }

double rgs_dchisq(SEXP node) {
	return dchisq(RGS_VAL[0], /*df*/ RGS_DPAR0[0], 1); }

double rgs_dexp(SEXP node) {
	return dexp(RGS_VAL[0], 1/ RGS_DPAR0[0], 1); }

double rgs_dlogis(SEXP node) {
	return dlogis(RGS_VAL[0], RGS_DPAR0[0], RGS_DPAR1[0], 1); }

double rgs_dbinom(SEXP node) {
	return dbinom(RGS_VAL[0], RGS_DPAR0[0], RGS_DPAR1[0], 1);}

double rgs_dgeom(SEXP node) {
	return dgeom(RGS_VAL[0], RGS_DPAR0[0], 1);}

double rgs_dunif(SEXP node) {
	return dunif(RGS_VAL[0], RGS_DPAR0[0], RGS_DPAR1[0], 1);}

double rgs_dpois(SEXP node) {
	return dpois(RGS_VAL[0], RGS_DPAR0[0], 1);}

double rgs_df(SEXP node) {
	return df(RGS_VAL[0], RGS_DPAR0[0], RGS_DPAR1[0], 1);}

double rgs_Rdistrib(SEXP node) {
	SEXP Rf_call, rans;
	double ans;
	SEXP distrArgs = VECTOR_ELT( RGS_NEXTRA(node) , 0 );
	SEXP rfun = VECTOR_ELT(distrArgs, 0);
	SEXP rho = VECTOR_ELT(distrArgs, 1);
	PROTECT( Rf_call = lang1( rfun ));
	PROTECT( rans = eval(Rf_call, rho) );
	ans = REAL(rans)[0];
	UNPROTECT(2);
	return ans;
}
