#include "rgs_mcmc.h"
#include <R_ext/Utils.h>

void GetRNGstate(void);
void PutRNGstate(void);

SEXP rgs_mcmc(SEXP nodeList, SEXP track, SEXP n, SEXP in_thin, SEXP in_necho){
	SEXP ans, tmp;
	int numNodes = length( nodeList );
	int numIter = INTEGER(n)[0];
	int thin = INTEGER(in_thin)[0];
	int necho = INTEGER(in_necho)[0];
	int nr = numIter / thin;
	int numTrack = length( track );
	int totSize = 0;
	pt2VFunc *smp = (pt2VFunc*) R_alloc(numNodes, sizeof(pt2VFunc));
	SEXP *node = (SEXP*) R_alloc(numNodes, sizeof(SEXP));
	for(int i=0; i<numNodes; i++) {
		node[i] = VECTOR_ELT(nodeList, i);
		smp[i] = RGS_CSAMPLER( node[i] );
	}
	for(int i=0; i< numTrack; i++)
		totSize += length( RGS_NVALUE ( node[ INTEGER(track)[i] ] ) );
	PROTECT( ans = allocMatrix(REALSXP, nr, totSize) );

	GetRNGstate();
	int ri = 0; int tmpCumulator;
	for(int i=0; i<numIter; i++) {
		R_CheckUserInterrupt();
		if(!((i+1) % necho))
			Rprintf("iteration %d\n", i+1);

		for(int j=0; j<numNodes; j++) /*call all samplers*/
			smp[j]( node[j] );

		if(!(i % thin)) { /*if thin it., store current values*/
			tmpCumulator = 0;
			for(int j=0; j<numTrack; j++) {
				tmp = node[ INTEGER(track)[j] ];
				for(int k=0; k<length( RGS_NVALUE( tmp ) ); k++)
					REAL(ans)[ri + nr * (k + tmpCumulator)] = REAL( RGS_NVALUE( tmp ) )[k];
				tmpCumulator += length( RGS_NVALUE( tmp ) );
			}
			ri++;
		}
	}
	PutRNGstate();

	UNPROTECT(1);
	return ans;
}
