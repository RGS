#ifndef __RGS_MH__
#define __RGS_MH__

#include "rgs_mcmc.h"

void rgs_mh(SEXP node);
void rgs_mh_norm_walk(SEXP node);

#endif
