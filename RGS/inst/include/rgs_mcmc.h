#ifndef __RGS_MCMC__
#define __RGS_MCMC__

#include <Rdefines.h>
#include <Rinternals.h>
#include "rgs_misc.h"
#include "rgs_node.h"
#include "rgs_distrib.h"

SEXP rgs_mcmc(SEXP nodeList, SEXP track, SEXP n, SEXP thin, SEXP necho);

#endif
