\name{getDistribution}
\alias{getDistribution}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ ~~function to do ... ~~ }
\description{
  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
getDistribution(model, nodeName)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{model}{ ~~Describe \code{model} here~~ }
  \item{nodeName}{ ~~Describe \code{nodeName} here~~ }
}
\details{
  ~~ If necessary, more details than the description above ~~
}
\value{
  ~Describe the value returned
  If it is a LIST, use
  \item{comp1 }{Description of 'comp1'}
  \item{comp2 }{Description of 'comp2'}
  ...
}
\references{ ~put references to the literature/web site here ~ }
\author{ ~~who you are~~ }
\note{ ~~further notes~~ 

 ~Make other sections like Warning with \section{Warning }{....} ~
}
\seealso{ ~~objects to See Also as \code{\link{help}}, ~~~ }
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function(model, nodeName) {
	node <- model[[nodeName]]
	function(x) {
		ov <- node$value
		on.exit(.Call("rgs_setValue", node, ov))
		.Call("rgs_setValue", node, as.real(x))
		.Call("rgs_getdistrib", node)
	}
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
