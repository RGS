\name{Node}
\alias{Node}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ ~~function to do ... ~~ }
\description{
  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
Node(value = NA, distrib = "", parameters = character(), sampler = "")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{value}{ ~~Describe \code{value} here~~ }
  \item{distrib}{ ~~Describe \code{distrib} here~~ }
  \item{parameters}{ ~~Describe \code{parameters} here~~ }
  \item{sampler}{ ~~Describe \code{sampler} here~~ }
}
\details{
  ~~ If necessary, more details than the description above ~~
}
\value{
  ~Describe the value returned
  If it is a LIST, use
  \item{comp1 }{Description of 'comp1'}
  \item{comp2 }{Description of 'comp2'}
  ...
}
\references{ ~put references to the literature/web site here ~ }
\author{ ~~who you are~~ }
\note{ ~~further notes~~ 

 ~Make other sections like Warning with \section{Warning }{....} ~
}
\seealso{ ~~objects to See Also as \code{\link{help}}, ~~~ }
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function(value=NA, distrib="", parameters=character(), sampler="") {
	ans <- list()
	ans$value <- value
	ans$distrib <- ""
	ans$parameters <- character()
	ans$extra <- list(list(), list())
	ans$cache <- list()
	ans$sampler <- ""

	stopifnot(all(sapply(parameters, is.character)))
	ans$parameters <- parameters

	class(ans) <- c('Node', class(ans))

	.setDistrib(ans, distrib)
	.setSampler(ans, sampler)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
